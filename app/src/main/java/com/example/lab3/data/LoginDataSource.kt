package com.example.lab3.data

import com.example.lab3.data.model.LoggedInUser
import java.io.IOException
import java.lang.RuntimeException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    private fun fakeAuthenticate(username: String, password: String): Unit {
        if(!username.equals("test") || !password.equals("test")) {
            throw RuntimeException()
        }
    }

    fun login(username: String, password: String): Result<LoggedInUser> {
        try {
            fakeAuthenticate(username, password)
            val fakeUser = LoggedInUser(java.util.UUID.randomUUID().toString(), "Jane Doe")
            return Result.Success(fakeUser)
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}